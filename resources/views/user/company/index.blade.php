@extends('user.layouts.app')

@section('content')
<section class="ec-page-content section-space-p">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2 class="ec-title">Tentang Kami</h2>
                    <p class="sub-title mb-3">Deskripsi cerita kami</p>
                </div>
            </div>
            <div class="ec-common-wrapper">
                <div class="row">
                    <div class="col-md-6 ec-cms-block ec-abcms-block text-center">
                        <div class="ec-cms-block-inner">
                        <img class="a-img" src="{{ asset('assets/images/banner/banner-1.jpg') }}" alt="about">
                        </div>
                    </div>
                    <div class="col-md-6 ec-cms-block ec-abcms-block text-center">
                        <div class="ec-cms-block-inner">
                            <h3 class="ec-cms-block-title">Lely Art Shop</h3>
                            <p>{{$company[0]->deskripsi}}</p>
                            <p>Visi : {{ $company[0]->visi }}</p>
                            <p>Misi : {{ $company[0]->misi }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ec-page-content section-space-p">
    <div class="container">
        <div class="row">
            <div class="ec-common-wrapper">
                <div class="ec_contact_map">
                    <div class="ec_map_canvas text-center">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3946.117154850494!2d119.88303517511265!3d-8.48798858588399!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2db4655da2eabf99%3A0xe3b00efd1239f7df!2sLely%20Art%20Shop!5e0!3m2!1sid!2sid!4v1715849599925!5m2!1sid!2sid" width="1000" height="500" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
                <div class="ec_contact_info">
                    <h1 class="ec_contact_info_head">Kontak Kami</h1>
                    <ul class="align-items-center">
                        <li class="ec-contact-item"><i class="ecicon eci-map-marker"
                                aria-hidden="true"></i><span>Address :</span>7{{ $company[0]->alamat }}</li>
                        <li class="ec-contact-item align-items-center"><i class="ecicon eci-phone"
                                aria-hidden="true"></i><span>Call Us :</span><a href="tel:+{{ $company[0]->telp }}">+{{ $company[0]->telp }}</a></li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection